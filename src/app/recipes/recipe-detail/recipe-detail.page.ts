import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RecipesService} from '../recipes.service';
import {Recipe} from '../recipe.model';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-recipe-detail',
    templateUrl: './recipe-detail.page.html',
    styleUrls: ['./recipe-detail.page.scss'],
})
export class RecipeDetailPage implements OnInit {
    loadedRecipes: Recipe;

    constructor(private activatedRoute: ActivatedRoute,
                private recipesService: RecipesService,
                private router: Router,
                private alertController: AlertController) {
    }

    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(paramMap => {
                if (!paramMap.has('recipeId')) {
                    this.router.navigate(['/recipes']);
                    return;
                }
                const recipeId = +paramMap.get('recipeId');
                this.loadedRecipes = this.recipesService.getRecipe(recipeId);
            },
            error => {
            });
    }

    onDeleteRecipe() {
        this.alertController.create({
            header: 'are you sure?',
            message: 'do you really want to delete recipe?',
            buttons:
                [{
                    text: 'Cancel',
                    role: 'cancel'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.recipesService.deleteRecipe(this.loadedRecipes.id);
                        this.router.navigate(['/recipes']);

                    }
                }]

        }).then(alertEL => {
            alertEL.present();
        });
    }
}
