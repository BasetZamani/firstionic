import {Injectable} from '@angular/core';
import {Recipe} from './recipe.model';

@Injectable({
    providedIn: 'root'
})
export class RecipesService {
    private recipes: Recipe[] = [
        {
            id: 1,
            title: 'baset',
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/43/Russia-Spain_2017_%286%29.jpg',
            ingredients: ['bnana ', 'kabab']
        },
        {
            id: 2,
            title: 'osameh',
            // tslint:disable-next-line:max-line-length
            imageUrl: 'https://db3ujbgzkl8k8.cloudfront.net/catalog/product/thumbnail/80686edd5d45963b4a45a8f73815b329/image/700x700/000/80/c/f/cf1203_1.jpg',
            ingredients: ['orange', 'joje']
        }
    ];

    constructor() {
    }

    getAllRecipes() {
        return [...this.recipes];
    }

    getRecipe(recipesId: number) {
        return {
            ...this.recipes.find(recipe => {
                return recipe.id === recipesId;
            })
        };
    }

    deleteRecipe(recipeId: number) {
        this.recipes = this.recipes.filter(recipe => {
            return recipe.id !== recipeId;
        });
    }
}
